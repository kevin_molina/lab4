package com.example.root.proyectopdm2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentLista.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class FragmentLista extends Fragment implements AdapterView.OnItemClickListener {

    private OnFragmentInteractionListener mListener;
    ListView lista;
    Activity activity;
    LugaresDB db;
    List<String> item= null;

    String consulta;


    public FragmentLista() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_fragment_lista, container, false);
        lista=(ListView)view.findViewById(R.id.list);
        //lista.setAdapter(new AdapterList(this, datos));
        consulta = String.valueOf(this.getArguments().getInt("consulta"));
        showLugares(consulta);
        lista.setOnItemClickListener(this);

       Log.v("id",consulta);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        if(position>=0){
            activity= getActivity();
            Intent intent = new Intent(activity, MapsActivity.class);
            intent.putExtra("consulta",consulta);
            startActivity(intent);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    public class AdapterList extends ArrayAdapter<Lugar> {
        Activity context;
        Lugar[] lista;

        AdapterList(Fragment context,Lugar[] lista) {
            super(context.getActivity(), R.layout.item, lista);
            this.context = context.getActivity();
            this.lista=lista;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View item = inflater.inflate(R.layout.item, null);

            TextView name = (TextView)item.findViewById(R.id.nombre);
            name.setText(lista[position].getName());



            return(item);
        }
    }
    private void showLugares(String consult){
        activity=getActivity();
        db= new LugaresDB(activity);
        Cursor c = db.getLocation(consult);
        item= new ArrayList<String>();
        String title="",content="";
        if (c.moveToFirst()){
            do {
                title= c.getString(1);
                content= c.getString(2);
                item.add(title+" ");


            }while (c.moveToNext());
        }
        ArrayAdapter<String> adaptador= new ArrayAdapter<String>(activity.getBaseContext(),android.R.layout.simple_list_item_1, item);
        Log.v("chumi", String.valueOf(adaptador));

        lista.setAdapter(adaptador);

    }
}
