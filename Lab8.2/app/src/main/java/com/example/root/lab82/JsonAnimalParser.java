package com.example.root.lab82;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class JsonAnimalParser {


    public List<Empresas> leerFlujoJson(InputStream in) throws IOException {
        // Nueva instancia JsonReader
        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
        try {
            // Leer Array
            return leerArrayAnimales(reader);
        } finally {
            reader.close();
        }

    }



    public List<Empresas> leerArrayAnimales(JsonReader reader) throws IOException {
        // Lista temporal
        ArrayList<Empresas> animales = new ArrayList<>();

        reader.beginArray();
        while (reader.hasNext()) {
            // Leer objeto
            animales.add(leerAnimal(reader));
        }
        reader.endArray();
        return animales;
    }

    public Empresas leerAnimal(JsonReader reader) throws IOException {
        // Variables locales
        String nombre = null;
        int id = 0;
        //String imagen = null;

        // Iniciar objeto
        reader.beginObject();

        /*
        Lectura de cada atributo
         */
        while (reader.hasNext()) {
            String name = reader.nextName();
            switch (name) {
                case "id":
                    id = reader.nextInt();

                    break;
                case "nombre":
                    nombre = reader.nextString();
                    break;

                default:
                    reader.skipValue();
                    break;
            }
        }
        reader.endObject();
        return new Empresas(id, nombre);
    }

}

