package com.example.root.lab44;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by root on 25/01/16.
 */
public class OnChargeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("App", "Recibido!");

        Toast.makeText(context, "Ha conectado el cargador.",Toast.LENGTH_SHORT).show();
    }
}
