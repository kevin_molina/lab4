package com.example.root.lab43;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btnSinHilos;
    private Button btnHilo;
    private Button btnAsyncTask;
    private Button btnCancelar;
    private Button btnAsyncDialog;
    private ProgressBar pbarProgreso;
    private ProgressDialog pDialog;
private MiTareaAsincrona tarea1;
private MiTareaAsincronaDialog tarea2;
    Button actividad3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSinHilos = (Button)findViewById(R.id.btnSinHilos);
        btnHilo = (Button)findViewById(R.id.button);
        btnAsyncTask = (Button)findViewById(R.id.button3);
        btnCancelar = (Button)findViewById(R.id.button4);
        btnAsyncDialog = (Button)findViewById(R.id.button5);
        pbarProgreso = (ProgressBar)findViewById(R.id.pbarProgreso);

        btnSinHilos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pbarProgreso.setMax(100); //valor máximo de ProgressBar
                pbarProgreso.setProgress(0); // Empieza vacía ProgressBar
                for(int i=1; i<=10; i++) {
                    tareaLarga();
                    pbarProgreso.incrementProgressBy(10); //incremento
                }
                Toast.makeText(MainActivity.this, "Tarea finalizada!",
                        Toast.LENGTH_SHORT).show();
            }
        });

        btnHilo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    public void run() {
                        pbarProgreso.post(new Runnable() {
                            public void run() {
                                pbarProgreso.setProgress(0);
                            }
                        });
                        for(int i=1; i<=10; i++) {
                            tareaLarga();
                            pbarProgreso.post(new Runnable() {
                                public void run() {
                                    pbarProgreso.incrementProgressBy(10);
                                }
                            });
                        }
                        runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(MainActivity.this, "Tarea finalizada!",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }).start();
                            }

        });
        final MiTareaAsincrona asin = new MiTareaAsincrona();

        btnAsyncTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                asin.onPreExecute();
                //asin.doInBackground();
                asin.onPostExecute(asin.doInBackground());
                btnCancelar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        asin.onCancelled();
                    }
                });
            }
        });
        btnAsyncDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDialog = new ProgressDialog(MainActivity.this);
                pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                pDialog.setMessage("Procesando...");
                pDialog.setCancelable(true);
                pDialog.setMax(100);
                tarea2 = new MiTareaAsincronaDialog();
                tarea2.execute();
            }
        });
        actividad3= (Button) findViewById(R.id.button2);
        actividad3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Actividad3.class);
                startActivity(intent);
            }
        });


    }



    private void tareaLarga() {
        try {
            Thread.sleep(1000);
        } catch(InterruptedException e) { }
    }
private class MiTareaAsincrona extends AsyncTask<Void,Integer,Boolean>{

    @Override
    protected Boolean doInBackground(Void... params) {

        for(int i=1; i<=10; i++) {
            tareaLarga();
            publishProgress(i*10);
            if(isCancelled())
                break;
        }
        return true;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pbarProgreso.setMax(100);
        pbarProgreso.setProgress(0);
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        if(aBoolean)
            Toast.makeText(MainActivity.this, "Tarea finalizada!",
                    Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        int progreso = values[0].intValue();
        pbarProgreso.setProgress(progreso);
    }

    @Override
    protected void onCancelled() {
        Toast.makeText(MainActivity.this, "Tarea cancelada!",
                Toast.LENGTH_SHORT).show();
    }
}



    private class MiTareaAsincronaDialog extends AsyncTask <Void,
            Integer, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {
            for (int i = 1; i <= 10; i++) {
                tareaLarga();
                publishProgress(i * 10);
                if (isCancelled())
                    break;
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progreso = values[0].intValue();
            pDialog.setProgress(progreso);
        }

        @Override
        protected void onPreExecute() {
            pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    MiTareaAsincronaDialog.this.cancel(true);
                }
            });
            pDialog.setProgress(0);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                pDialog.dismiss();
                Toast.makeText(MainActivity.this, "Tarea finalizada!",
                        Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(MainActivity.this, "Tarea cancelada!",
                    Toast.LENGTH_SHORT).show();
        }
    }
}