package com.example.root.proyectopdm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

public class Visita extends AppCompatActivity implements View.OnClickListener{

    TextView nombre;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visita);
    int posicion;
        posicion= getIntent().getIntExtra("PosVisita",-1);
        nombre=(TextView)findViewById(R.id.nombre_lugar);
    nombre.setText(String.valueOf(posicion));
        //Toolbar tb= (Toolbar)findViewById(R.id)
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Toast.makeText(this, "Action Settings selected", Toast.LENGTH_SHORT)
                        .show();
                // User chose the "Settings" item, show the app settings UI...
                return true;

            case R.id.action_favorite:
                Toast.makeText(this, "Inicia Audio Guia", Toast.LENGTH_SHORT)
                        .show();
                // User chose the "Favorite" action, mark the current item
                // as a favorite...
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.action_favorite:
                Intent intent = new Intent(Visita.this,Multimedia.class);
                startActivity(intent);
                break;
            default:
                break;

        }
    }
}
