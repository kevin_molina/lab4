package com.example.root.proyectopdm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
ImageButton gps;
    ListView listView;
    public static final String[] Lugares={"Plaza de Armas", "Catedral"};
    public static final String[] restaurant={"La Romana", "Mamatere"};
    public static final String[] museo={"Museo Nacional", "Museo Italiano"};
    //public static final String[] centro={"Plaza de Armas", "C"};
    public static final String[] hotel={"Marriot", "Sheraton"};
    public static final String[] huecos={"Cucardas", "Conejas"};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView= (ListView)findViewById(R.id.listView);
        gps=(ImageButton)findViewById(R.id.imageButton);
        ArrayAdapter<String> adapter;
        int posicion= getIntent().getIntExtra("PosVisita",-1);
        switch (posicion){
            case 1:  adapter= new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, restaurant);
                break;
            case 3: adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, restaurant);
                break;
            case 4: adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, hotel);
                break;
            case 5: adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, museo);
              break;
            case 6: adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, Lugares);
                break;
            case 7: adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, huecos);
                break;
            default:
            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, Lugares);
                break;
        }

        listView.setAdapter(adapter);

        gps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                startActivity(intent);
                //AIzaSyDC2BP5xVVqPKq_FeZjEJUnDvu9yy7_yMM

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getApplicationContext(), ""+parent.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, Visita.class);
                Log.d("gg", "" + parent.getItemAtPosition(position));
                intent.putExtra("PosVisita", position);

                startActivity(intent);
            }
        });




    }
}
