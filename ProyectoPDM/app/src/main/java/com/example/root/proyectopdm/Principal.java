package com.example.root.proyectopdm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

public class Principal extends AppCompatActivity implements View.OnClickListener{

    ImageButton ubicacion;

    ImageButton rest;
    ImageButton hoteles;
    ImageButton museos;
    ImageButton centroHis;
    ImageButton entretenimiento;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        rest= (ImageButton)findViewById(R.id.rest);
        ubicacion= (ImageButton)findViewById(R.id.ubc);
        hoteles= (ImageButton)findViewById(R.id.hot);
        museos= (ImageButton)findViewById(R.id.muse);
        centroHis= (ImageButton)findViewById(R.id.tur);
        entretenimiento= (ImageButton)findViewById(R.id.xxx);

        rest.setOnClickListener(this);
        ubicacion.setOnClickListener(this);
        hoteles.setOnClickListener(this);
        museos.setOnClickListener(this);
        centroHis.setOnClickListener(this);
        entretenimiento.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rest:
                Intent intent = new Intent(this, MainActivity.class);
                Log.d("gg", "1" );
                intent.putExtra("PosVisita", 1);
                startActivity(intent);
                break;
            case R.id.ubc:
                Intent intent3 = new Intent(this, MapsActivity.class);
                Log.d("gg", "3" );
                intent3.putExtra("PosVisita", 3);

                startActivity(intent3);
                break;
            case R.id.hot:
                Intent intent4 = new Intent(this, MainActivity.class);
                Log.d("gg", "4" );
                intent4.putExtra("PosVisita", 4);

                startActivity(intent4);
                break;
            case R.id.muse:
                Intent intent5 = new Intent(this, MainActivity.class);
                Log.d("gg", "5" );
                intent5.putExtra("PosVisita", 5);

                startActivity(intent5);
                break;
            case R.id.tur:
                Intent intent6 = new Intent(this, MainActivity.class);
                Log.d("gg", "6" );
                intent6.putExtra("PosVisita", 6);

                startActivity(intent6);
                break;
            case R.id.xxx:
                Intent intent7 = new Intent(this, MainActivity.class);
                Log.d("gg", "7" );
                intent7.putExtra("PosVisita", 7);

                startActivity(intent7);
                break;
            default:break;

        }

    }
}
