package com.example.root.lab62;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Actividad4 extends AppCompatActivity {
    private ImageView animationTarget;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad4);

        animationTarget= (ImageView)findViewById(R.id.imageView2);

    }

    public void rota (View v){
        Animator animation = AnimatorInflater.loadAnimator(this, R.anim.rotate_around_center_point);
        animation.setTarget(animationTarget);
        animation.start();
    }
}
