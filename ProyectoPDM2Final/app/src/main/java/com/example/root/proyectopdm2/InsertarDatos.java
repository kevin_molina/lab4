package com.example.root.proyectopdm2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class InsertarDatos extends AppCompatActivity {

    EditText nombre,Desc, cat,lat,longi;
    Button add;
    String nom,desc,lati, lo;
    String cate;
    LugaresDB db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insertar_datos);

        nombre=(EditText)findViewById(R.id.editTextid);
        Desc= (EditText)findViewById(R.id.editTextdes);
        cat=(EditText)findViewById(R.id.editTextcat);
        add=(Button)findViewById(R.id.button);
        lat=(EditText)findViewById(R.id.editTextLat);
        longi=(EditText)findViewById(R.id.editTextLong);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            addDatos();
            }
        });
    }
    private void addDatos(){
        nom=nombre.getText().toString();
        desc= Desc.getText().toString();
        cate= cat.getText().toString();
        lati= lat.getText().toString();
        lo= longi.getText().toString();

        db= new LugaresDB(this);
        db.addLugar(nom, desc, cate, lati,lo,"1");

        //db.close();
        finish();

    }
}
