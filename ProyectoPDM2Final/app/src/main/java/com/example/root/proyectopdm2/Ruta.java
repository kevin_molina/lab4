package com.example.root.proyectopdm2;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by root on 11/01/16.
 */
public class Ruta {
    String resumen;
    LatLng startLocation;
    LatLng endLocation;
    ArrayList<LatLng> steps;
    ArrayList<String> stepEncodedPolylines;
    String encodedPolyline;
    ArrayList<Integer> durationValues;
    int tiempoEstimado;
    ArrayList<String> instructions;
    ArrayList<Integer> distanceValues;
    int distanciaEstimada;

    public Ruta() {
    }

    @Override
    public String toString() {
        return "Ruta{" +
                "resumen='" + resumen + '\'' +
                ", startLocation=" + startLocation +
                ", endLocation=" + endLocation +
                ", steps=" + steps +
                ", stepEncodedPolylines=" + stepEncodedPolylines +
                ", encodedPolyline='" + encodedPolyline + '\'' +
                ", durationValues=" + durationValues +
                ", tiempoEstimado=" + tiempoEstimado +
                ", instructions=" + instructions +
                ", distanceValues=" + distanceValues +
                ", distanciaEstimada=" + distanciaEstimada +
                '}';
    }

    public String getResumen() {
        return resumen;
    }

    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    public LatLng getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(LatLng startLocation) {
        this.startLocation = startLocation;
    }

    public LatLng getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(LatLng endLocation) {
        this.endLocation = endLocation;
    }

    public ArrayList<LatLng> getSteps() {
        return steps;
    }

    public void setSteps(ArrayList<LatLng> steps) {
        this.steps = steps;
    }

    public ArrayList<String> getStepEncodedPolylines() {
        return stepEncodedPolylines;
    }

    public void setStepEncodedPolylines(ArrayList<String> stepEncodedPolylines) {
        this.stepEncodedPolylines = stepEncodedPolylines;
    }

    public String getEncodedPolyline() {
        return encodedPolyline;
    }

    public void setEncodedPolyline(String encodedPolyline) {
        this.encodedPolyline = encodedPolyline;
    }

    public ArrayList<Integer> getDurationValues() {
        return durationValues;
    }

    public void setDurationValues(ArrayList<Integer> durationValues) {
        this.durationValues = durationValues;
    }

    public int getTiempoEstimado() {
        return tiempoEstimado;
    }

    public void setTiempoEstimado(int tiempoEstimado) {
        this.tiempoEstimado = tiempoEstimado;
    }

    public ArrayList<String> getInstructions() {
        return instructions;
    }

    public void setInstructions(ArrayList<String> instructions) {
        this.instructions = instructions;
    }

    public ArrayList<Integer> getDistanceValues() {
        return distanceValues;
    }

    public void setDistanceValues(ArrayList<Integer> distanceValues) {
        this.distanceValues = distanceValues;
    }

    public int getDistanciaEstimada() {
        return distanciaEstimada;
    }

    public void setDistanciaEstimada(int distanciaEstimada) {
        this.distanciaEstimada = distanciaEstimada;
    }
}
