package com.example.root.proyectopdm2;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;


    public LatLng AMSTERDAM = new LatLng(-12.041091876269444, -76.8937886506319);//entrada UNInicio
    public LatLng PARIS = new LatLng(-12.016741, -77.049984);//entrada CTICFinal
    private static final LatLng FRANKFURT = new LatLng(-12.019196, -77.056352);//admision
    private LatLngBounds latlngBounds;
    private Polyline newPolyline;
    private ArrayList<Polyline> misPolylines;
    private ArrayList<Marker> misMarkers;
    private boolean isTravelingToParis = false;
    private int width, height;
    TextToSpeech tts;
    private SupportMapFragment fragment;
    private GoogleMap map;
    public LatLng ubi;
    LugaresDB db;
    ///

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);


        getSreenDimanstions();
        fragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
        map = fragment.getMap();
        misPolylines = new ArrayList<Polyline>();
        misMarkers = new ArrayList<Marker>();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        // SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
        //       .findFragmentById(R.id.map);
        //mapFragment.getMapAsync(this);
        fragment.getMapAsync(this);
        db= new LugaresDB(this);
        SQLiteDatabase database=db.getReadableDatabase();
        Bundle consult= getIntent().getExtras();
        getPosicion();
        //
        AMSTERDAM= ubi;


        Cursor fila= db.getPosition(consult.getString("id"));
        if (fila.moveToFirst()) {
            do {

          PARIS= new LatLng(fila.getFloat(0),fila.getFloat(1));


            } while(fila.moveToNext());

        }

      database.close();
        if (!isTravelingToParis) {
            isTravelingToParis = true;
            findDirections(AMSTERDAM.latitude, AMSTERDAM.longitude, PARIS.latitude, PARIS.longitude, GMapV2Direction.MODE_WALKING);

        } else {
            isTravelingToParis = false;
            findDirections(PARIS.latitude, PARIS.longitude, FRANKFURT.latitude, FRANKFURT.longitude, GMapV2Direction.MODE_WALKING);
        }

        //Log.v("chui", String.valueOf(map.getMyLocation().getLatitude()));
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
       // Log.v("chui", String.valueOf(mMap.getMyLocation().getLatitude()));

    }




///////

    public void findDirections(double fromPositionDoubleLat, double fromPositionDoubleLong, double toPositionDoubleLat, double toPositionDoubleLong, String mode)
    {
        Map<String, String> map = new HashMap<String, String>();
        map.put(GetDirectionsAsyncTask.USER_CURRENT_LAT, String.valueOf(fromPositionDoubleLat));
        map.put(GetDirectionsAsyncTask.USER_CURRENT_LONG, String.valueOf(fromPositionDoubleLong));
        map.put(GetDirectionsAsyncTask.DESTINATION_LAT, String.valueOf(toPositionDoubleLat));
        map.put(GetDirectionsAsyncTask.DESTINATION_LONG, String.valueOf(toPositionDoubleLong));
        map.put(GetDirectionsAsyncTask.DIRECTIONS_MODE, mode);

        GetDirectionsAsyncTask asyncTask = new GetDirectionsAsyncTask(this);
        asyncTask.execute(map);
    }

    public void handleGetRoutesResult(ArrayList<Ruta> rutas){

        for(int pol=0; pol<misPolylines.size(); pol++){
            misPolylines.get(pol).remove();
        }

        misPolylines.clear();

        for(int mar=0; mar<misMarkers.size(); mar++){
            misMarkers.get(mar).remove();
        }

        misMarkers.clear();

        if (isTravelingToParis)
        {
            latlngBounds = createLatLngBoundsObject(AMSTERDAM, PARIS);
            map.animateCamera(CameraUpdateFactory.newLatLngBounds(latlngBounds, width, height, 150));
        }
        else
        {

            latlngBounds = createLatLngBoundsObject(AMSTERDAM, FRANKFURT);
            map.animateCamera(CameraUpdateFactory.newLatLngBounds(latlngBounds, width, height, 150));
        }

        for(int x=0; x<rutas.size(); x++){
            PolylineOptions rectLine = new PolylineOptions().width(5).color(Color.rgb(100 * x, 200 * x, x));
            MarkerOptions markerOptions = new MarkerOptions().title("Semaforo");
            ArrayList<LatLng> polylinepoints = decodePoly(rutas.get(x).encodedPolyline);
            for(int i=0; i<polylinepoints.size(); i++){
                rectLine.add(polylinepoints.get(i));
            }
            misPolylines.add(map.addPolyline(rectLine));

            ArrayList<LatLng> semaforosPositions = rutas.get(x).getSteps();
            for(int sema = 0; sema< semaforosPositions.size(); sema++){
               // misMarkers.add(map.addMarker(new MarkerOptions().position(semaforosPositions.get(sema))));
            }

        }

//        startTalking(rutas);
    }

    private LatLngBounds createLatLngBoundsObject(LatLng firstLocation, LatLng secondLocation)
    {
        if (firstLocation != null && secondLocation != null)
        {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(firstLocation).include(secondLocation);

            return builder.build();
        }
        return null;
    }

    private ArrayList<LatLng> decodePoly(String encoded) {
        ArrayList<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;
        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;
            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng position = new LatLng((double) lat / 1E5, (double) lng / 1E5);
            poly.add(position);
        }
        return poly;
    }

    public void startTalking(ArrayList<Ruta> rutas){
        ArrayList<Integer> intervalosTiempo = rutas.get(0).getDurationValues();
        ArrayList<String> indicaciones = rutas.get(0).getInstructions();
        ArrayList<Integer> longitudes = rutas.get(0).getDistanceValues();

        Log.d("masrutas", indicaciones.toString());
        for(int i=0; i<intervalosTiempo.size(); i++){
            Log.d("pasoss",intervalosTiempo.get(i)+"");
//            tts.speak(String.valueOf(Html.fromHtml(indicaciones.get(i))), TextToSpeech.QUEUE_ADD, null);
            tts.playSilence(intervalosTiempo.get(i)*1000,TextToSpeech.QUEUE_ADD,null);
            Log.d("pasoss",indicaciones.get(i)+"");
        }

    }
    private void getSreenDimanstions()
    {
        Display display = getWindowManager().getDefaultDisplay();
        width = display.getWidth();
        height = display.getHeight();
    }

    public void getPosicion(){
        GPSService mGPSService = new GPSService(this);
        mGPSService.getLocation();
        String address;


        if (mGPSService.isLocationAvailable == false) {

            // Here you can ask the user to try again, using return; for that
            Toast.makeText(this, "Your location is not available, please try again.", Toast.LENGTH_SHORT).show();
            return;

            // Or you can continue without getting the location, remove the return; above and uncomment the line given below
            // address = "Location not available";
        } else {

            // Getting location co-ordinates
            double latitude = mGPSService.getLatitude();
            double longitude = mGPSService.getLongitude();
           // Toast.makeText(this, "Latitude:" + latitude + " | Longitude: " + longitude, Toast.LENGTH_LONG).show();

            address = mGPSService.getLocationAddress();
            ubi= new LatLng(latitude, longitude);
            //tvLocation.setText("Latitude: " + latitude + " \nLongitude: " + longitude);
            //tvAddress.setText("Address: " + address);
        }

        Toast.makeText(this, "Tu direccion es: " + address, Toast.LENGTH_SHORT).show();

        // make sure you close the gps after using it. Save user's battery power
        mGPSService.closeGPS();




    }
}
