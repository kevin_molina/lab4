package com.example.root.proyectopdm2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by root on 03/03/16.
 */
public class LugaresDB extends SQLiteOpenHelper {
    public static final String Table="Lugares";
    public static final String DATABASE="pruebaAndroid";

    public LugaresDB(Context context) {
        super(context, DATABASE, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Table + " (Id INTEGER PRIMARY KEY AUTOINCREMENT, Nombre TEXT ,Descripcion TEXT ,Categoria INTEGER, Latitud FLOAT, Longitud FLOAT, Foto TEXT)");
        db.execSQL("INSERT INTO Lugares(Id, Nombre, Descripcion , Categoria ,Latitud, Longitud, Foto) VALUES(10, 'Plaza San Martin', 'La Plaza San Martín es uno de los espacios públicos más representativos de la ciudad de Lima, Perú. Está ubicada en la cuadra 9 de la Avenida Colmena en pleno centro histórico de Lima dentro del área declarada en 1988 por la Unesco como Patrimonio de la Humanidad.',3,-12.051623800420153, -77.03460864722729 , '@drawable/f1')");
        db.execSQL("INSERT INTO Lugares(Id, Nombre, Descripcion , Categoria ,Latitud, Longitud, Foto) VALUES(11, 'Pizza Hut','Pizza Hut es una franquicia estadounidense de restaurantes de comida rápida, especializada en la elaboración de pizzas.', 1, -12.05220088021366, -77.03417044132948,'@drawable/f3')");
        db.execSQL("INSERT INTO Lugares(Id, Nombre, Descripcion , Categoria ,Latitud, Longitud, Foto) VALUES(13, 'Museo de Arte de Lima','El Palacio de la Exposición, edificio que alberga al Museo de Arte de Lima - MALI, es uno de los ejemplos más bellos de la arquitectura ecléctica limeña. Concebido y construido como sede de la primera gran exposición pública en nuestro país, la Gran Muestra de Artes, Ciencias e Industrias, llevada a cabo con motivo de los cincuenta años de independencia, se adecúa fácilmente a las funciones del museo, ya que fue construido expresamente para fines expositivos. Fue edificado entre los años 1870 y 1871 durante el gobierno de José Balta. Se trata de un edificio precursor en América Latina, pues es una de las más tempranas e importantes obras hechas con la nueva técnica de construcción en fierro. Proyectado en el estilo neo-renacentista, su diseño y construcción se deben al arquitecto italiano Antonio Leonardi. La fabricación de las columnas, hechas en fierro e importadas desde Europa, es atribuida a la casa Eiffel. Rodeado por estatuas, jardines y un zoológico, el Palacio fue el corazón de uno de los proyectos urbanos más importantes del siglo pasado, siguiendo el ejemplo de las exposiciones universales europeas.', 2, -12.060448395215333, -77.03697770833969, '@drawable/f4')");
        db.execSQL("INSERT INTO Lugares(Id, Nombre, Descripcion , Categoria ,Latitud, Longitud, Foto) VALUES(14, 'Las Cucardas', 'Es un centro de diversion para adultos con mas de 50 anos a su servicio, donde podra realizar sus mas anhelada fantasias en comparnia de bellas anfitrionas dentro de un ambiente comodo y agradable.' , 5, -12.045594226030797, -77.05452170222998, '@drawable/f5')");
        db.execSQL("INSERT INTO Lugares(Id, Nombre, Descripcion , Categoria ,Latitud, Longitud, Foto) VALUES(15, 'Sheraton','Desde su primer hotel en 1937, Sheraton Hotels & Resorts ha sido una figura transformadora en el mundo de los viajes. La programación innovadora, los destinos internacionales y el compromiso con nuestros huéspedes nos han permitido permanecer a la vanguardia de la industria por más de 70 años. Siempre estamos actualizándonos, adaptándonos y cambiando con los tiempos; sin embargo, en nuestro núcleo, seguimos siendo fieles a los valores que comenzaron nuestro recorrido hace tantos años atrás.', 4, -12.057269948247752, -77.03686974942684, '@drawable/f6')");
        db.execSQL("INSERT INTO Lugares(Id, Nombre, Descripcion , Categoria ,Latitud, Longitud, Foto) VALUES(16, 'Circuito Magico del Agua','En un inicio, el lugar donde se extiende actualmente el parque perteneció al Parque de la Exposición. A fines de 1926 se inició su construcción, ordenada por el presidente Augusto B. Leguía. Lo bautizaron como de la Reserva en honor de las tropas reservistas que participaron durante la Guerra del Pacífico en la defensa de la ciudad de Lima en las batallas de San Juan y Miraflores. El parque fue culminado en 1929.', 3, -12.071090049268316, -77.03416876494884, '@drawable/f7')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+Table);
        onCreate(db);
    }

    public void close(){
        this.close();

    }
    public Cursor getLugares(){
        String Columnas[]={"Id","Nombre","Latitud","Longitud", "Foto"};
        Cursor c=  this.getReadableDatabase().query(Table,Columnas,null,null,null,null,null);
        return c;

    }
    public void addLugar(String nombre, String Desc, String cat, String lat, String longi, String fot ){

        ContentValues valores= new ContentValues();
        valores.put("Nombre",nombre);
        valores.put("Descripcion", Desc);
        valores.put("Categoria", cat);
        valores.put("Latitud",lat);
        valores.put("Longitud", longi);
        valores.put("Foto", fot);
        this.getWritableDatabase().insert(Table,null , valores);
    }
    public Cursor getLocation(String a){
        Cursor c=  this.getWritableDatabase().rawQuery("select Id, Nombre,Latitud, Longitud, Foto from Lugares where Categoria=" + a, null);
        return c;
    }
    public Cursor getRepro(String a){
        Cursor c=  this.getWritableDatabase().rawQuery("select Nombre, Descripcion ,Foto, id from Lugares where Nombre= " +"'"+ a+"'", null);
        return c;
    }
    public Cursor getPosition(String a){
        Cursor c=  this.getWritableDatabase().rawQuery("select Latitud, Longitud from Lugares where Id=" + a, null);
        return c;


    }
}
