package com.example.root.proyectopdm2;

import android.os.Environment;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Created by root on 07/01/16.
 */
public class GMapV2Direction {
    public final static String MODE_DRIVING = "driving";
    public final static String MODE_WALKING = "walking";
    boolean espera=false;
    private int result;
    TextToSpeech tts;

    public GMapV2Direction(){
    }


    public Document getDocument(LatLng start, LatLng end, String mode) {
        String url = "http://maps.googleapis.com/maps/api/directions/xml?"
                + "origin=" + start.latitude + "," + start.longitude
                + "&destination=" + end.latitude + "," + end.longitude
                + "&sensor=true&units=metric&alternatives=false&mode="+mode+"&language=es";


        Log.d("url",url);

        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();
            HttpPost httpPost = new HttpPost(url);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            InputStream in = response.getEntity().getContent();
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(in);
            getDurationText(doc);
            //getManeuverText(doc);
            printXML(doc);

            /*NodeList nodeList = doc.getElementsByTagName("html_instructions");
            NodeList nodeListDistancia = doc.getElementsByTagName("distance");
            Log.d("indicaciones", nodeList.getLength()+"");

            for(int i=0; i<nodeList.getLength(); i++){
                Log.d("indicaciones", nodeList.item(i).getTextContent()+" "+nodeListDistancia.item(i).getChildNodes().item(1).getTextContent()+" metros");
            }*/
            showRoutes(doc);
            ArrayList<Ruta> blindRoutes = readRoutes(doc);
            for(int x=0; x<blindRoutes.size(); x++){
                Log.d("misrutas","ruta #"+x);
                Log.d("misrutas",blindRoutes.get(x).getInstructions().toString());
                ArrayList<String> instrucciones = blindRoutes.get(x).getInstructions();

                for(String step : instrucciones){
                    Log.d("instrucciones", step);
                    //hablo(step,espera);
                    //tts = new TextToSpeech(context, this);
                    //int result = tts.setLanguage(Locale.getDefault());
                    //tts.speak(step, TextToSpeech.QUEUE_FLUSH, null);
                }
            }
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void showRoutes(Document doc){
        NodeList rutas = doc.getElementsByTagName("route");

        for(int i=0; i<rutas.getLength(); i++){
            Log.d("route","///////////////////////////////");
            Node nodoRuta = rutas.item(i);
            NodeList listaNodosRuta = nodoRuta.getChildNodes();
            for(int j=0; j<listaNodosRuta.getLength(); j++){
                if(listaNodosRuta.item(j).getNodeType()==1){
                    Node nodoRuta1 = listaNodosRuta.item(j);
                    Log.d("route",nodoRuta1.getNodeName());
                    NodeList lista = nodoRuta1.getChildNodes();
                    for(int k=0; k<lista.getLength(); k++){
                        if(lista.item(k).getNodeType()==1){
                            Log.d("route", "\t"+lista.item(k).getNodeName());
                            Node nodoRuta2 = lista.item(k);
                            NodeList lista2 = nodoRuta2.getChildNodes();
                            for(int l=1; l<lista2.getLength(); l++){
                                if(lista2.item(l).getNodeType()==1){
                                    Log.d("route", "\t\t"+lista2.item(l).getNodeName());
                                    Node nodoRuta3 = lista2.item(l);
                                    NodeList lista3 = nodoRuta3.getChildNodes();
                                    for(int m=0; m<lista3.getLength(); m++){
                                        if(lista3.item(m).getNodeType()==1){
                                            Log.d("route","\t\t\t"+lista3.item(m).getNodeName());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public ArrayList<Ruta> readRoutes(Document doc){
        ArrayList<Ruta> misRutas = new ArrayList<>();
        NodeList rutas = doc.getElementsByTagName("route");

        Log.d("infoRutas", "Tengo "+rutas.getLength()+" posibles rutas");

        for(int rutaIndex=0; rutaIndex<rutas.getLength(); rutaIndex++){
            Ruta ruta = new Ruta();
            ArrayList<LatLng> steps = new ArrayList<LatLng>();
            ArrayList<String> stepEndodedPolylines = new ArrayList<String>();
            ArrayList<Integer> durationValues = new ArrayList<Integer>();
            ArrayList<String> instrucciones = new ArrayList<String>();
            ArrayList<Integer> distanceValues = new ArrayList<>();

            Node nodoRuta = rutas.item(rutaIndex);
            NodeList rutaLabels = nodoRuta.getChildNodes();
            for(int labelIndex=0; labelIndex<rutaLabels.getLength(); labelIndex++){
                if(rutaLabels.item(labelIndex).getNodeType() == Node.ELEMENT_NODE){
                    Log.d("rutaLev1",rutaLabels.item(labelIndex).getNodeName());
                    Node labelLevelOne = rutaLabels.item(labelIndex);
                    NodeList nodosLevelTwo = labelLevelOne.getChildNodes();

                    if(labelLevelOne.getNodeName().equalsIgnoreCase("summary")){
                        ruta.setResumen(labelLevelOne.getTextContent());
                    }

                    else if(labelLevelOne.getNodeName().equalsIgnoreCase("leg")){
                        for(int labelIndexLevelTwo = 0; labelIndexLevelTwo< nodosLevelTwo.getLength(); labelIndexLevelTwo++){
                            if(nodosLevelTwo.item(labelIndexLevelTwo).getNodeType() == Node.ELEMENT_NODE){
                                Log.d("rutaLev2","\t"+nodosLevelTwo.item(labelIndexLevelTwo).getNodeName());
                                Node labelLevelTwo = nodosLevelTwo.item(labelIndexLevelTwo);
                                NodeList nodosLevelThree = labelLevelTwo.getChildNodes();

                                if(labelLevelTwo.getNodeName().equalsIgnoreCase("step")){//analizar un paso, sirve para los nodos
                                    for(int labelIndexLevelThree=0; labelIndexLevelThree<nodosLevelThree.getLength(); labelIndexLevelThree++){
                                        if(nodosLevelThree.item(labelIndexLevelThree).getNodeType() == Node.ELEMENT_NODE){
                                            Node labelLevelThree = nodosLevelThree.item(labelIndexLevelThree);
                                            NodeList nodosLevelFourth = labelLevelThree.getChildNodes();
                                            Log.d("rutaLev21","\t\t"+labelLevelThree.getNodeName());
                                            if(labelLevelThree.getNodeName().equalsIgnoreCase("travel_mode")){
                                                Log.d("travel_mode", labelLevelThree.getTextContent());
                                            }
                                            else if(labelLevelThree.getNodeName().equalsIgnoreCase("start_location")){
                                                double lat, lng;
                                                lat = 1.0;
                                                lng = 1.0;
                                                for(int labelIndexLevelFourth=0; labelIndexLevelFourth<nodosLevelFourth.getLength(); labelIndexLevelFourth++){
                                                    Node labelLevelFourth = nodosLevelFourth.item(labelIndexLevelFourth);
                                                    if(labelLevelFourth.getNodeType() == Node.ELEMENT_NODE){

                                                        Log.d("rutaLev211","\t\t\t\t" +labelLevelFourth.getNodeName());
                                                        if(labelLevelFourth.getNodeName().equals("lat")){
                                                            lat = Double.parseDouble(labelLevelFourth.getTextContent());
                                                        }
                                                        if(labelLevelFourth.getNodeName().equals("lng")){
                                                            lng = Double.parseDouble(labelLevelFourth.getTextContent());
                                                        }
                                                    }
                                                }
                                                steps.add(new LatLng(lat, lng));//no considera el nodo de llegada, lo podemos agregar del otro objeto
                                            }
                                            else if(labelLevelThree.getNodeName().equalsIgnoreCase("end_location")){
                                                double lat, lng;
                                                lat = lng = 1.0;

                                                for(int labelIndexLevelFourth=0; labelIndexLevelFourth<nodosLevelFourth.getLength(); labelIndexLevelFourth++){
                                                    Node labelLevelFourth = nodosLevelFourth.item(labelIndexLevelFourth);
                                                    if(labelLevelFourth.getNodeType() == Node.ELEMENT_NODE){
                                                        Log.d("rutaLev211", "\t\t\t\t"+labelLevelFourth.getNodeName());
                                                        if(labelLevelFourth.getNodeName().equals("lat")){
                                                            lat = Double.parseDouble(labelLevelFourth.getTextContent());
                                                        }
                                                        else if(labelLevelFourth.getNodeName().equals("lng")){
                                                            lng = Double.parseDouble(labelLevelFourth.getTextContent());
                                                        }
                                                        //solo por si acaso, aqui puedo hacer cualquier otra cosa xD
                                                    }
                                                }
                                            }
                                            else if(labelLevelThree.getNodeName().equalsIgnoreCase("polyline")){
                                                for(int labelIndexLevelFourth=0; labelIndexLevelFourth<nodosLevelFourth.getLength(); labelIndexLevelFourth++){
                                                    Node labelLevelFourth = nodosLevelFourth.item(labelIndexLevelFourth);
                                                    if(labelLevelFourth.getNodeType() == Node.ELEMENT_NODE){
                                                        Log.d("rutaLev211", "\t\t\t\t"+labelLevelFourth.getNodeName());
                                                        if(labelLevelFourth.getNodeName().equals("points")){
                                                            Log.d("subpolylines", labelLevelFourth.getTextContent());
                                                            stepEndodedPolylines.add(labelLevelFourth.getTextContent());
                                                        }
                                                    }
                                                }
                                            }
                                            else if(labelLevelThree.getNodeName().equalsIgnoreCase("duration")){
                                                for(int labelIndexLevelFourth=0; labelIndexLevelFourth<nodosLevelFourth.getLength(); labelIndexLevelFourth++){
                                                    Node labelLevelFourth = nodosLevelFourth.item(labelIndexLevelFourth);
                                                    if(labelLevelFourth.getNodeType() == Node.ELEMENT_NODE){
                                                        Log.d("rutaLev211", "\t\t\t\t"+labelLevelFourth.getNodeName());
                                                        if(labelLevelFourth.getNodeName().equals("value")){
                                                            durationValues.add(Integer.parseInt(labelLevelFourth.getTextContent()));
                                                        }
                                                        else if(labelLevelFourth.getNodeName().equals("text")){

                                                        }
                                                    }
                                                }
                                            }
                                            else if(labelLevelThree.getNodeName().equalsIgnoreCase("html_instructions")){
                                                instrucciones.add(labelLevelThree.getTextContent());
                                            }
                                            else if(labelLevelThree.getNodeName().equalsIgnoreCase("distance")){
                                                for(int labelIndexLevelFourth=0; labelIndexLevelFourth<nodosLevelFourth.getLength(); labelIndexLevelFourth++){
                                                    Node labelLevelFourth = nodosLevelFourth.item(labelIndexLevelFourth);
                                                    if(labelLevelFourth.getNodeType() == Node.ELEMENT_NODE){
                                                        Log.d("rutaLev211", "\t\t\t\t"+labelLevelFourth.getNodeName());
                                                        if(labelLevelFourth.getNodeName().equals("value")){
                                                            distanceValues.add(Integer.parseInt(labelLevelFourth.getTextContent()));
                                                        }
                                                        else if(labelLevelFourth.getNodeName().equals("text")){

                                                        }
                                                    }
                                                }
                                            }
                                            else if(labelLevelThree.getNodeName().equalsIgnoreCase("maneuver")){

                                            }
                                        }
                                    }
                                }
                                else if(labelLevelTwo.getNodeName().equalsIgnoreCase("duration")){
                                    for(int labelIndexLevelThree=0; labelIndexLevelThree<nodosLevelThree.getLength(); labelIndexLevelThree++){
                                        if(nodosLevelThree.item(labelIndexLevelThree).getNodeType() == Node.ELEMENT_NODE){
                                            Node labelLevelThree = nodosLevelThree.item(labelIndexLevelThree);
                                            Log.d("rutaLev21", "\t\t" + labelLevelThree.getNodeName());
                                            if(labelLevelThree.getNodeName().equalsIgnoreCase("value")){
                                                ruta.setTiempoEstimado(Integer.parseInt(labelLevelThree.getTextContent()));
                                            }
                                            else if(labelLevelThree.getNodeName().equalsIgnoreCase("text")){
                                                Log.d("textoDuracion",labelLevelThree.getTextContent());
                                            }
                                        }
                                    }
                                }
                                else if(labelLevelTwo.getNodeName().equalsIgnoreCase("distance")){
                                    for(int labelIndexLevelThree=0; labelIndexLevelThree<nodosLevelThree.getLength(); labelIndexLevelThree++){
                                        if(nodosLevelThree.item(labelIndexLevelThree).getNodeType() == Node.ELEMENT_NODE){
                                            Node labelLevelThree = nodosLevelThree.item(labelIndexLevelThree);
                                            Log.d("rutaLev21","\t\t"+labelLevelThree.getNodeName());
                                            if(labelLevelThree.getNodeName().equalsIgnoreCase("value")){
                                                ruta.setDistanciaEstimada(Integer.parseInt(labelLevelThree.getTextContent()));
                                            }
                                            else if(labelLevelThree.getNodeName().equalsIgnoreCase("text")){
                                                Log.d("textoDistancia", labelLevelThree.getTextContent());
                                            }
                                        }
                                    }
                                }
                                else if(labelLevelTwo.getNodeName().equalsIgnoreCase("start_location")){
                                    Double latitude, lng;
                                    latitude = 1.0;
                                    lng = 1.0;

                                    for(int labelIndexLevelThree=0; labelIndexLevelThree<nodosLevelThree.getLength(); labelIndexLevelThree++){
                                        if(nodosLevelThree.item(labelIndexLevelThree).getNodeType() == Node.ELEMENT_NODE){
                                            Node labelLevelThree = nodosLevelThree.item(labelIndexLevelThree);
                                            Log.d("rutaLev21","\t\t"+labelLevelThree.getNodeName());
                                            if(labelLevelThree.getNodeName().equals("lat")){
                                                latitude = Double.parseDouble(labelLevelThree.getTextContent());
                                                Log.d("start_location", latitude+"MiLatitud");
                                            }
                                            if(labelLevelThree.getNodeName().equals("lng")){
                                                Log.d("start_location", labelLevelThree.getTextContent());
                                                lng = Double.parseDouble(labelLevelThree.getTextContent());
                                            }
                                            LatLng startLocation = new LatLng(latitude, lng);
                                            Log.d("start_location", startLocation.toString());

                                        }
                                    }
                                    ruta.setStartLocation(new LatLng(latitude, lng));
                                }
                                else if(labelLevelTwo.getNodeName().equalsIgnoreCase("end_location")){
                                    double lat, lng;
                                    lat = 1.0;
                                    lng = 1.0;

                                    for(int labelIndexLevelThree=0; labelIndexLevelThree<nodosLevelThree.getLength(); labelIndexLevelThree++){
                                        if(nodosLevelThree.item(labelIndexLevelThree).getNodeType() == Node.ELEMENT_NODE){
                                            Node labelLevelThree = nodosLevelThree.item(labelIndexLevelThree);
                                            Log.d("rutaLev21","\t\t"+labelLevelThree.getNodeName());
                                            if(labelLevelThree.getNodeName().equals("lat")){
                                                lat = Double.parseDouble(labelLevelThree.getTextContent());
                                            }
                                            if(labelLevelThree.getNodeName().equals("lng")){
                                                lng = Double.parseDouble(labelLevelThree.getTextContent());
                                            }
                                        }
                                    }
                                    ruta.setEndLocation(new LatLng(lat, lng));
                                }
                                else if(labelLevelTwo.getNodeName().equalsIgnoreCase("start_address")){
                                    Log.d("startaddress", labelLevelTwo.getTextContent());
                                }
                                else if(labelLevelTwo.getNodeName().equalsIgnoreCase("end_address")){
                                    Log.d("endaddress", labelLevelTwo.getTextContent());
                                }

                            }
                        }
                    }
                    else if(labelLevelOne.getNodeName().equalsIgnoreCase("copyrights")){
                        Log.d("copyrights", labelLevelOne.getTextContent());//useless for my ruta Object
                    }

                    else if(labelLevelOne.getNodeName().equalsIgnoreCase("overview_polyline")){

                        for(int labelIndexLevelTwo = 0; labelIndexLevelTwo< nodosLevelTwo.getLength(); labelIndexLevelTwo++){
                            if(nodosLevelTwo.item(labelIndexLevelTwo).getNodeType() == Node.ELEMENT_NODE){
                                Log.d("rutaLev2","\t"+nodosLevelTwo.item(labelIndexLevelTwo).getNodeName());
                                Node labelLevelTwo = nodosLevelTwo.item(labelIndexLevelTwo);
                                NodeList nodosLevelThree = labelLevelTwo.getChildNodes();
                                if(labelLevelTwo.getNodeName().equalsIgnoreCase("points")){
                                    ruta.setEncodedPolyline(labelLevelTwo.getTextContent());
                                }
                            }
                        }
                    }
                    else if(labelLevelOne.getNodeName().equalsIgnoreCase("warning")){
                        Log.d("warning", labelLevelOne.getTextContent());
                    }
                    else if(labelLevelOne.getNodeName().equalsIgnoreCase("bounds")){
                        for(int labelIndexLevelTwo = 0; labelIndexLevelTwo< nodosLevelTwo.getLength(); labelIndexLevelTwo++){
                            if(nodosLevelTwo.item(labelIndexLevelTwo).getNodeType() == Node.ELEMENT_NODE){
                                Log.d("rutaLev2","\t"+nodosLevelTwo.item(labelIndexLevelTwo).getNodeName());
                                Node labelLevelTwo = nodosLevelTwo.item(labelIndexLevelTwo);
                                NodeList nodosLevelThree = labelLevelTwo.getChildNodes();
                                if(labelLevelTwo.getNodeName().equalsIgnoreCase("southwest")){
                                    for(int labelIndexLevelThree=0; labelIndexLevelThree<nodosLevelThree.getLength(); labelIndexLevelThree++){
                                        if(nodosLevelThree.item(labelIndexLevelThree).getNodeType() == Node.ELEMENT_NODE){
                                            Node labelLevelThree = nodosLevelThree.item(labelIndexLevelThree);
                                            Log.d("rutaLev21", "\t\t" + labelLevelThree.getNodeName());
                                            if(labelLevelThree.getNodeName().equalsIgnoreCase("lat")){
                                                Log.d("southwest",labelLevelThree.getTextContent());
                                            }
                                            else if(labelLevelThree.getNodeName().equalsIgnoreCase("lng")){
                                                Log.d("southwest",labelLevelThree.getTextContent());
                                            }
                                        }
                                    }
                                }
                                else if(labelLevelTwo.getNodeName().equalsIgnoreCase("northeast")){
                                    for(int labelIndexLevelThree=0; labelIndexLevelThree<nodosLevelThree.getLength(); labelIndexLevelThree++){
                                        if(nodosLevelThree.item(labelIndexLevelThree).getNodeType() == Node.ELEMENT_NODE){
                                            Node labelLevelThree = nodosLevelThree.item(labelIndexLevelThree);
                                            Log.d("rutaLev21","\t\t"+labelLevelThree.getNodeName());
                                            if(labelLevelThree.getNodeName().equalsIgnoreCase("lat")){
                                                Log.d("northeast",labelLevelThree.getTextContent());
                                            }
                                            else if(labelLevelThree.getNodeName().equalsIgnoreCase("lng")){
                                                Log.d("northeast",labelLevelThree.getTextContent());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }


                }
            }
            ruta.setSteps(steps);
            ruta.setStepEncodedPolylines(stepEndodedPolylines);
            ruta.setDistanceValues(distanceValues);
            ruta.setDurationValues(durationValues);
            ruta.setInstructions(instrucciones);
            misRutas.add(ruta);
        }
        return misRutas;
    }

    public void printXML(Document document) throws TransformerException {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(document), new StreamResult(writer));
        String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
        guardarTodo(output);
    }

    public void guardarTodo(String myxml){

        try{
                File storageDir = new File(Environment.getExternalStorageDirectory()+"");
                storageDir.mkdirs();
                File file = new File(storageDir,"respuesta.txt");
                FileOutputStream fos = new FileOutputStream(file);
                fos.write((myxml).getBytes());
                fos.close();
            } catch(Exception e){
                Log.d("texto", "no se pudo guardar por x razonnes");
                e.printStackTrace();
            }

    }

    public String getDurationText (Document doc) {
        NodeList nl1 = doc.getElementsByTagName("duration");
        Node node1 = nl1.item(0);
        NodeList nl2 = node1.getChildNodes();
        Node node2 = nl2.item(getNodeIndex(nl2, "text"));
        Log.i("DurationText", node2.getTextContent());
        return node2.getTextContent();
    }

    public int getDurationValue (Document doc) {
        NodeList nl1 = doc.getElementsByTagName("duration");
        Node node1 = nl1.item(0);
        NodeList nl2 = node1.getChildNodes();
        Node node2 = nl2.item(getNodeIndex(nl2, "value"));
        Log.i("DurationValue", node2.getTextContent());
        return Integer.parseInt(node2.getTextContent());
    }

    public String getDistanceText (Document doc) {
        NodeList nl1 = doc.getElementsByTagName("distance");
        Node node1 = nl1.item(0);
        NodeList nl2 = node1.getChildNodes();
        Node node2 = nl2.item(getNodeIndex(nl2, "text"));
        Log.i("DistanceText", node2.getTextContent());
        return node2.getTextContent();
    }

    public int getDistanceValue (Document doc) {
        NodeList nl1 = doc.getElementsByTagName("distance");
        Node node1 = nl1.item(0);
        NodeList nl2 = node1.getChildNodes();
        Node node2 = nl2.item(getNodeIndex(nl2, "value"));
        Log.i("DistanceValue", node2.getTextContent());
        return Integer.parseInt(node2.getTextContent());
    }

    public String getStartAddress (Document doc) {
        NodeList nl1 = doc.getElementsByTagName("start_address");
        Node node1 = nl1.item(0);
        Log.i("StartAddress", node1.getTextContent());
        return node1.getTextContent();
    }

    public String getEndAddress (Document doc) {
        NodeList nl1 = doc.getElementsByTagName("end_address");
        Node node1 = nl1.item(0);
        Log.i("StartAddress", node1.getTextContent());
        return node1.getTextContent();
    }

    public String getCopyRights (Document doc) {
        NodeList nl1 = doc.getElementsByTagName("copyrights");
        Node node1 = nl1.item(0);
        Log.i("CopyRights", node1.getTextContent());
        return node1.getTextContent();
    }

    public ArrayList<LatLng> getDirection (Document doc) {
        NodeList nl1, nl2, nl3;
        ArrayList<LatLng> listGeopoints = new ArrayList<LatLng>();
        nl1 = doc.getElementsByTagName("step");
        if (nl1.getLength() > 0) {
            for (int i = 0; i < nl1.getLength(); i++) {
                Node node1 = nl1.item(i);
                nl2 = node1.getChildNodes();

                Node locationNode = nl2.item(getNodeIndex(nl2, "start_location"));
                nl3 = locationNode.getChildNodes();
                Node latNode = nl3.item(getNodeIndex(nl3, "lat"));
                double lat = Double.parseDouble(latNode.getTextContent());
                Node lngNode = nl3.item(getNodeIndex(nl3, "lng"));
                double lng = Double.parseDouble(lngNode.getTextContent());
                listGeopoints.add(new LatLng(lat, lng));

                locationNode = nl2.item(getNodeIndex(nl2, "polyline"));
                nl3 = locationNode.getChildNodes();
                latNode = nl3.item(getNodeIndex(nl3, "points"));
                ArrayList<LatLng> arr = decodePoly(latNode.getTextContent());
                for(int j = 0 ; j < arr.size() ; j++) {
                    listGeopoints.add(new LatLng(arr.get(j).latitude, arr.get(j).longitude));
                }

                locationNode = nl2.item(getNodeIndex(nl2, "end_location"));
                nl3 = locationNode.getChildNodes();
                latNode = nl3.item(getNodeIndex(nl3, "lat"));
                lat = Double.parseDouble(latNode.getTextContent());
                lngNode = nl3.item(getNodeIndex(nl3, "lng"));
                lng = Double.parseDouble(lngNode.getTextContent());
                listGeopoints.add(new LatLng(lat, lng));
            }
        }

        return listGeopoints;
    }

    private int getNodeIndex(NodeList nl, String nodename) {
        for(int i = 0 ; i < nl.getLength() ; i++) {
            if(nl.item(i).getNodeName().equals(nodename))
                return i;
        }
        return -1;
    }

    private ArrayList<LatLng> decodePoly(String encoded) {
        ArrayList<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;
        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;
            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng position = new LatLng((double) lat / 1E5, (double) lng / 1E5);
            poly.add(position);
        }
        return poly;
    }


    private void speakOut(String texto) {
        tts.speak(texto, TextToSpeech.QUEUE_FLUSH, null);
    }
}
