package com.example.root.proyectopdm2;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Locale;

public class Visita extends AppCompatActivity implements TextToSpeech.OnInitListener {
    private ImageView speechButton;
    private TextToSpeech engine;
    private double pitch=1.0;
    private double speed=1.0;
    LugaresDB db;
    String Reproducir, Desc;
    TextView nombreT,DescricionT;
    ImageView foto, ruta;
    String Id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visita);
        nombreT = (TextView)findViewById(R.id.nombre_lugar);
        DescricionT=(TextView)findViewById(R.id.textDescripcion);
        foto=(ImageView)findViewById(R.id.imageView);
        ruta=(ImageView)findViewById(R.id.ImagenIr);
        db= new LugaresDB(this);
        SQLiteDatabase database=db.getReadableDatabase();
        Bundle nombre= getIntent().getExtras();
        Cursor fila= db.getRepro(nombre.getString("tit"));
        if (fila.moveToFirst()) {
            do {
                Reproducir= fila.getString(1);
                Log.v("db", fila.getString(0));
                String uri = fila.getString(2);
                Id=fila.getString(3);
                int resId = this.getResources().getIdentifier(uri,null,this.getPackageName());

                foto.setImageResource(resId);


            } while(fila.moveToNext());

        }
        nombreT.setText(nombre.getString("tit"));
        DescricionT.setText(Reproducir);

        database.close();

        speechButton = (ImageView) findViewById(R.id.imageView2);
        engine = new TextToSpeech(this, this);

        speechButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                speech(Reproducir);
            }
        });

        ruta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Visita.this, MapsActivity.class);
                intent.putExtra("id", Id );
                startActivity(intent);
            }
        });




    }

    @Override
    public void onInit(int status) {
        Log.d("Speech", "OnInit - Status [" + status + "]");

        if (status == TextToSpeech.SUCCESS) {
            Log.d("Speech", "Success!");
            engine.setLanguage(new Locale( "spa", "ESP" ));
            //engine.setLanguage(Locale.US);
        }
    }
    private void speech(String a) {
        engine.setPitch(0.0f);
        engine.setSpeechRate(0.0f);
        engine.speak(a, TextToSpeech.QUEUE_FLUSH, null);

    }
}
