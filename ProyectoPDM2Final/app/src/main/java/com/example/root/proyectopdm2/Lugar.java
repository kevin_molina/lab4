package com.example.root.proyectopdm2;

/**
 * Created by root on 02/03/16.
 */
public class Lugar {
    private String name;
    private String apellidos;
    private String descripcion;



    public Lugar(String name, String apellidos, String descripcion) {
        super();
        this.name = name;
        this.apellidos = apellidos;
        this.descripcion = descripcion;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getApellidos() {
        return apellidos;
    }
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
