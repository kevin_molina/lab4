package com.example.root.lab5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
Button guardar;
    Button atras;
    EditText texto;
    ToggleButton toggleButton;
    CheckBox checkBox;
    RadioButton RBizq;
    RadioButton RBder;
    RatingBar ratingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        guardar= (Button)findViewById(R.id.salvar);
        atras = (Button)findViewById(R.id.volver);
        texto= (EditText)findViewById(R.id.editText);
        checkBox=(CheckBox)findViewById(R.id.checkBox);
        toggleButton=(ToggleButton)findViewById(R.id.toggleButton);
        RBder=(RadioButton)findViewById(R.id.radioButton2);
        RBizq= (RadioButton)findViewById(R.id.radioButton);
        ratingBar=(RatingBar)findViewById(R.id.ratingBar);

        guardar.setOnClickListener(this);
        atras.setOnClickListener(this);
        RBder.setOnClickListener(this);
        RBizq.setOnClickListener(this);
        checkBox.setOnClickListener(this);
        guardar.setEnabled(false);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.salvar:
                String allText = new String("campo:" + texto.getText());
                allText = allText + ":checkbox:";
                if (checkBox.isChecked()) {
                    allText = allText + "Checked:";
                } else {
                    allText = allText + "Not Checked:";
                }
                allText = allText + ":toggle:";
                if (toggleButton.isChecked()) {
                    allText = allText + "Checked:";
                } else {
                    allText = allText + "Not Checked:";
                }
                allText = allText + "radios:rojo:";
                String redtext = "";
                if (RBder.isChecked()) {
                    redtext = "pulsado:";
                } else {
                    redtext = "no pulsado:";
                }
                allText = allText + redtext;
                allText = allText + "azul";
                String bluetext = "";
                if (RBizq.isChecked()) {
                    bluetext = "pulsado:";
                } else {
                    bluetext = "no pulsado:";
                }
                allText = allText + bluetext;
                allText = allText + "rating:";
                float f = ratingBar.getRating();
                allText = allText + Float.toString( f ) + ":";
                Log.d("app", allText);
                Toast.makeText(this, allText, Toast.LENGTH_LONG).show();

                break;

            case R.id.radioButton:
                Toast.makeText(this,RBizq.getText(),Toast.LENGTH_LONG).show();

                break;
            case R.id.radioButton2:
                Toast.makeText(this,RBder.getText(), Toast.LENGTH_LONG).show();
                break;
            case R.id.volver:
                finish();
                break;
            case R.id.checkBox:
                String text = "";
                // guardar.setEnabled(false);
                if (checkBox.isChecked()) {
                    text = "Selected";
                    guardar.setEnabled(true);
                    Toast.makeText(this,"Ya puedes Salvar",
                            Toast.LENGTH_LONG).show();
                } else {
                    guardar.setEnabled(false);
                    Toast.makeText(this, "Hasta que no marques la casilla no podrás salvar",Toast.LENGTH_LONG).show();
                    text = "Not selected";
                }
                Toast.makeText(this,text, Toast.LENGTH_SHORT).show();
            default:
                break;


        }
    }


    public void checkBoxClick(View v) {

    }





        }

